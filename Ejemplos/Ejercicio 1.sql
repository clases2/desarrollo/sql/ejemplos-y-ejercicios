﻿CREATE DATABASE IF NOT EXISTS ejercicio1;

USE ejercicio1;

CREATE TABLE IF NOT EXISTS depart(
  dept_no int(11),
  dnombre varchar(30),
  loc varchar(30),
  PRIMARY KEY (dept_no)
);

CREATE TABLE IF NOT EXISTS emple(
  emp_no int(11),
  apellido varchar(50) NOT NULL,
  oficio varchar(30),
  dir int(11),
  fecha_alt date,
  salario int(11),
  comision int(11),
  dept_no int(11),
  PRIMARY KEY(emp_no),
  FOREIGN KEY(dept_no) REFERENCES depart(dept_no)
);

INSERT INTO depart (dept_no,dnombre,loc) VALUES
  (10,'Contabilidad','Sevilla'),
  (20,'Investigacion','Madrid'),
  (30,'Ventas','Barcelona'),
  (40,'Produccion','Bilbao');

INSERT INTO emple (emp_no,apellido,oficio,dir,fecha_alt,salario,comision,dept_no) VALUES
  (7369,'Sanchez','Empelado',7902,'1990/12/17',1040,NULL,20),
  (7499,'Arroyo','Vendedor',7698,'1990/02/20',1500,390,30),
  (7521,'Sala','Vendedor',7698,'1991/02/22',1625,650,30),
  (7566,'Jimenez','Director',7839,'1991/04/02',2900,NULL,20),
  (7654,'Martin','Vendedor',7698,'1991/09/29',1600,1020,30),
  (7698,'Negro','Director',7839,'1991/05/01',3005,NULL,30),
  (7782,'Cerezo','Director',7839,'1991/06/09',2885,NULL,10),
  (7788,'Gil','Analista',7566,'1991/11/09',3000,NULL,20),
  (7839,'Rey','Presidente',NULL,'1991/11/17',4100,NULL,10),
  (7844,'Tovar','Vendedor',7698,'1991/09/08',1350,0,30),
  (7876,'Alonso','Empleado',7788,'1991/09/23',1430,NULL,20),
  (7900,'Jimeno','Empleado',7698,'1991/12/03',1335,NULL,30),
  (7902,'Fernandez','Analista',7566,'1991/12/03',3000,NULL,20),
  (7934,'Muñoz','Empleado',7782,'1991/01/23',1690,NULL,10);
