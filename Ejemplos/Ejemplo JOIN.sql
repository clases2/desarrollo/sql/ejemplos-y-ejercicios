﻿/* SELECT m.nombre, m.fechanac, t.nombre, t.provincia FROM mitabla m INNER JOIN tciudad t ON t.idciudad = m.ciudad; */

/* Muestra codigo y nombre de todas las personas nacidas fuera de la provincia de Madrid. */

  SELECT m.cod, m.nombre, t.provincia FROM mitabla m INNER JOIN tciudad t ON t.idciudad = m.ciudad WHERE t.provincia!="madrid";