﻿CREATE DATABASE IF NOT EXISTS VideoClub_Cantabria;

USE VideoClub_Cantabria;

CREATE TABLE IF NOT EXISTS Peliculas(
  codigopelicula varchar(5),
  titulo varchar (40) NOT NULL,
  genero varchar (15),
  año int,
  pais varchar (15),
  PRIMARY KEY(codigopelicula)
);

CREATE TABLE IF NOT EXISTS Socios(
  numerosocio varchar(5),
  dni varchar(10) NOT NULL UNIQUE,
  nombre varchar(20),
  apellidos varchar(40),
  direccion varchar(50),
  telefono varchar(10),
  fechanacimiento date,
  PRIMARY KEY(numerosocio)
);

CREATE TABLE IF NOT EXISTS Prestamos(
  codigoprestamo int,
  numerosocio varchar(5),
  codigopelicula varchar(5),
  PRIMARY KEY(codigoprestamo),
  FOREIGN KEY(numerosocio) REFERENCES Socios(numerosocio),
  FOREIGN KEY(codigopelicula) REFERENCES Peliculas(codigopelicula)
);

INSERT INTO Peliculas (codigopelicula,titulo,genero,año) VALUES
  ('P001','Rebelion en la Ondas','Comedia',1987),
  ('P002','Rocky','Drama',1976),
  ('P003','El Exorcista','Terror',1973),
  ('P004','Ronin','Policiaca',1998),
  ('P005','Rocky II','Drama',1979),
  ('P006','Cyrano de Bergeraç','Drama',1990),
  ('P007','Rocky III','Drama',1982),
  ('P008','El Resplandor','Terror',1980),
  ('P009','Sin Perdon','Western',1992);

INSERT INTO Socios (numerosocio,dni,nombre,apellidos,direccion,telefono,fechanacimiento) VALUES
    ('S001','10101010A','Fernando','Alonso','Calle Rueda','985332211','1980/06/02'),
    ('S002','20202020B','Elmer','Benett','Avda Castilla','915852266','1985/12/05'),
    ('S003','30303030C','Sofia','Loren','Plaza Mayor','942335544','1903/08/07'),
    ('S004','40404040D','Mar','Flores','Calle Alegria','914587895','1987/05/31'),
    ('S005','50505050E','Tamara','Torres','Calle Perejil','935784578','1975/03/24'),
    ('S006','11111111F','Federico','Trillo','Calle Honduras','942225544','1950/04/29'),
    ('S007','11223344G','Marina','Castaño','Avda Jeta','916589878','1957/01/15'),
    ('S008','72727272H','Diego','Tristan','Calle Galicia','942353535','1979/10/19'),
    ('S009','25252525J','Andrei','Shevchenko','Calle Milan','915544545','1982/03/26');

INSERT INTO Prestamos (codigoprestamo,numerosocio,codigopelicula) VALUES
  (001,'S001','P001'),
  (002,'S002','P003'),
  (003,'S003','P004'),
  (004,'S005','P006'),
  (005,'S007','P001'),
  (006,'S008','P002'),
  (007,'S008','P003'),
  (008,'S009','P001'),
  (009,'S004','P005'),
  (010,'S006','P007'),
  (011,'S001','P008'),
  (012,'S002','P009');