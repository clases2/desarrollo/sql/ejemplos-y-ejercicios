﻿CREATE DATABASE IF NOT EXISTS mibasededatos;

USE mibasededatos;

CREATE TABLE IF NOT EXISTS tciudad(
  idciudad varchar(3),
  nombre varchar(30),
  provincia varchar(30),
  PRIMARY KEY(idciudad)
);

CREATE TABLE IF NOT EXISTS mitabla(
  cod int,
  nombre varchar(30),
  fechanac date,
  ciudad varchar(3) NOT NULL,
  PRIMARY KEY(cod),
  FOREIGN KEY(ciudad) REFERENCES tciudad(idciudad)
);

INSERT INTO tciudad (idciudad,nombre,provincia) VALUES 
  (1,'ciudad1','provincia1'),
  (2,'ciudad2','provincia2'),
  (3,'ciudad3','provincia3'),
  (4,'ciudad4','provincia4'),
  (5,'ciudad5','provincia5');

INSERT INTO mitabla (cod,nombre,fechanac,ciudad) VALUES
  (1,'nombre1','2022-06-10',1),
  (2,'nombre2','2022-06-10',2),
  (3,'nombre3','2022-06-10',3),
  (4,'nombre4','2022-06-10',4),
  (5,'nombre5','2022-06-10',5);
