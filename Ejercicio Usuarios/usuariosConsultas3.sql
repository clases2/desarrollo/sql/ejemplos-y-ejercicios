# Listar el login de los usuarios con nivel 1 o 3
SELECT usuario, nivel FROM tblusuarios WHERE nivel=1 OR nivel=3;

# Listar nombre y tel�fono de los usuarios con tel�fono que no sea de la marca BLACKBERRY 
SELECT nombre, telefono, marca FROM tblusuarios WHERE marca!="blackberry";

# Listar el login de los usuarios con nivel 3
SELECT usuario, nivel FROM tblusuarios WHERE nivel=3;

# Listar el login de los usuarios con nivel 0
SELECT usuario, nivel FROM tblusuarios WHERE nivel=0;

# Listar el login de los usuarios con nivel 1
SELECT usuario, nivel FROM tblusuarios WHERE nivel=1;

# Contar el n�mero de usuarios por sexo
SELECT COUNT(*),sexo FROM tblusuarios GROUP BY sexo;

# Listar el login y tel�fono de los usuarios con compa�ia telef�nica AT&T
SELECT usuario, telefono, compa�ia FROM tblusuarios WHERE compa�ia="at&t";

# Listar las diferentes compa�ias en orden alfab�tico descendentemente
SELECT compa�ia FROM tblusuarios GROUP BY compa�ia ORDER BY marca DESC;

# Listar el login de los usuarios inactivos
SELECT usuario, activo FROM tblusuarios WHERE activo=0;

# Listar los n�meros de tel�fono sin saldo
SELECT telefono, saldo FROM tblusuarios WHERE saldo=0;

# Calcular el saldo m�nimo de los usuarios de sexo "Hombre"
SELECT MIN(saldo), sexo FROM tblusuarios ORDER BY sexo;

# Listar los n�meros de tel�fono con saldo mayor a 300
SELECT telefono, saldo FROM tblusuarios WHERE saldo>=300;