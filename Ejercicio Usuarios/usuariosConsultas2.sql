# Listar nombre y tel�fono de los usuarios con tel�fono que no sea de la marca LG o SAMSUNG
SELECT nombre, telefono, marca FROM tblusuarios WHERE marca!="samsung" AND marca!="lg";

# Listar el login y tel�fono de los usuarios con compa�ia telef�nica IUSACELL
SELECT usuario, telefono, compa�ia FROM tblusuarios WHERE compa�ia="iusacell";

# Listar el login y tel�fono de los usuarios con compa�ia telef�nica que no sea TELCEL
SELECT usuario, telefono, compa�ia FROM tblusuarios WHERE compa�ia!="telcel";

# Calcular el saldo promedio de los usuarios que tienen tel�fono marca NOKIA
SELECT AVG(saldo), marca FROM tblusuarios WHERE marca="nokia";

# Listar el login y tel�fono de los usuarios con compa�ia telef�nica IUSACELL o AXEL
SELECT usuario, telefono, compa�ia FROM tblusuarios WHERE compa�ia="iusacell" OR compa�ia="axel";

# Mostrar el email de los usuarios que no usan yahoo
SELECT nombre, email FROM tblusuarios WHERE email NOT LIKE "%yahoo%";

# Listar el login y tel�fono de los usuarios con compa�ia telef�nica que no sea TELCEL o IUSACELL
SELECT usuario, telefono, compa�ia FROM tblusuarios WHERE compa�ia!="telcel" AND compa�ia!="iusacell";

# Listar el login y tel�fono de los usuarios con compa�ia telef�nica UNEFON
SELECT usuario, telefono, compa�ia FROM tblusuarios WHERE compa�ia!="unefon";

# Listar las diferentes marcas de celular en orden alfab�tico descendentemente
SELECT marca FROM tblusuarios GROUP BY marca ORDER BY marca;

# Listar las diferentes compa�ias en orden alfab�tico aleatorio
SELECT marca FROM tblusuarios GROUP BY marca ORDER BY RAND();

# Listar el login de los usuarios con nivel 0 o 2
SELECT usuario, nivel FROM tblusuarios WHERE nivel=1 or nivel=2;

# Calcular el saldo promedio de los usuarios que tienen tel�fono marca LG
SELECT AVG(saldo), marca FROM tblusuarios WHERE marca="lg";