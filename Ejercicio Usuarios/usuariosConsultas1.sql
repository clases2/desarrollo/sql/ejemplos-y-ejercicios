# Listar los nombres de los usuarios
SELECT idx,nombre FROM tblusuarios t;

# Calcular el saldo m�ximo de los usuarios de sexo "Mujer"
SELECT MAX(t.saldo) FROM tblusuarios t WHERE t.sexo="m";

#Listar nombre y tel�fono de los usuarios con tel�fono NOKIA, BLACKBERRY o SONY
SELECT nombre, telefono FROM tblusuarios WHERE marca="nokia" OR marca="blackberry" OR marca="sony";

#Contar los usuarios sin saldo o inactivos
SELECT usuario, saldo, activo FROM tblusuarios WHERE saldo=0 OR activo=0;

#Listar el usuario de los usuarios con nivel 1, 2 o 3
SELECT usuario, nivel FROM tblusuarios WHERE nivel>=1 AND nivel<=3;

#Listar los n�meros de tel�fono con saldo menor o igual a 300
SELECT telefono, saldo FROM tblusuarios WHERE saldo<=300;

#Calcular la suma de los saldos de los usuarios de la compa�ia telef�nica NEXTEL
SELECT SUM(saldo) FROM tblusuarios WHERE compa�ia="nextel";

#Contar el n�mero de usuarios por compa��a telef�nica AVISAR

SELECT t.compa�ia, COUNT(*) FROM tblusuarios t GROUP BY t.compa�ia;

#Contar el n�mero de usuarios por nivel
SELECT t.nivel, COUNT(*) FROM tblusuarios t GROUP BY t.nivel;

#Listar el login de los usuarios con nivel 2
SELECT usuario, nivel FROM tblusuarios WHERE nivel=2;

#Mostrar el email de los usuarios que usan gmail
SELECT usuario, email FROM tblusuarios WHERE email LIKE "%gmail%";

#Listar nombre y tel�fono de los usuarios con tel�fono LG, SAMSUNG o MOTOROLA
  SELECT nombre, telefono, marca FROM tblusuarios WHERE marca="lg" or marca="samsung" or marca="motorola";

