# Contar el n�mero de usuarios por marca de tel�fono
SELECT COUNT(*), marca FROM tblusuarios GROUP BY marca;

# Listar nombre y tel�fono de los usuarios con tel�fono que no sea de la marca LG
SELECT nombre, telefono, marca FROM tblusuarios WHERE marca!="lg";

# Listar las diferentes compa�ias en orden alfab�tico ascendentemente
SELECT compa�ia FROM tblusuarios GROUP BY compa�ia ORDER BY marca;

# Calcular la suma de los saldos de los usuarios de la compa�ia telef�nica UNEFON
SELECT SUM(saldo), compa�ia FROM tblusuarios WHERE compa�ia="unefon" GROUP BY compa�ia;

# Mostrar el email de los usuarios que usan hotmail
SELECT nombre, email FROM tblusuarios WHERE email LIKE "%hotmail%";

# Listar los nombres de los usuarios sin saldo o inactivos
SELECT nombre, saldo, activo FROM tblusuarios WHERE saldo=0 OR activo=0;

# Listar el login y tel�fono de los usuarios con compa�ia telef�nica IUSACELL o TELCEL
SELECT usuario, telefono, compa�ia FROM tblusuarios WHERE compa�ia="iusacell" OR compa�ia="telcel" ORDER BY compa�ia;

# Listar las diferentes marcas de celular en orden alfab�tico ascendentemente
SELECT marca FROM tblusuarios GROUP BY marca ORDER BY marca;

# Listar las diferentes marcas de celular en orden alfab�tico aleatorio
SELECT marca FROM tblusuarios GROUP BY marca ORDER BY RAND();

# Listar el login y tel�fono de los usuarios con compa�ia telef�nica IUSACELL o UNEFON
SELECT usuario, telefono, compa�ia FROM tblusuarios WHERE compa�ia="iusacell" OR compa�ia="unefon" ORDER BY compa�ia;

# Listar nombre y tel�fono de los usuarios con tel�fono que no sea de la marca MOTOROLA o NOKIA
SELECT nombre, telefono, marca FROM tblusuarios WHERE marca!="motorola" AND marca!="nokia" ORDER BY marca;

# Calcular la suma de los saldos de los usuarios de la compa�ia telef�nica TELCEL
SELECT SUM(saldo), compa�ia FROM tblusuarios WHERE compa�ia="telcel" GROUP BY compa�ia;