﻿CREATE TABLE IF NOT EXISTS Contactos(
  numcontacto int,
  nombre text(20),
  apellidos text(40),
  fechanac date,
  telefono text(9) NOT NULL DEFAULT "000000000",
  PRIMARY KEY (numerocontacto)
);

CREATE TABLE IF NOT EXISTS Clientes(
  codcliente int,
  nombre text(20),
  apellidos text(40),
  DNI text(10),
  descuento numeric DEFAULT "5",
  tarjetasocio boolean,
  PRIMARY KEY (codcliente)
);

INSERT INTO Contactos (numcontacto, nombre, apellidos, fechanac, telefono) VALUES (
  (1, "Antonio", "Garcia Perez", "1960/08/15", "942369521"),
  (2, "Carlos", "Perez Ruiz", "1958/04/26", "942245147"),
  (3, "Luis", "Rodriguez Mas", "1961/03/30", "942296578"),
  (4, "Jaime", "Juangran Sornes", "1968/01/31", "942368496"),
  (5, "Alfonso", "Prats Montolla", "1969/04/28", "942354852"),
  (6, "Jose", "Navarro Lard", "1964/05/15", "942387469"),
  (7, "Elisa", "Ubeda Sanson", "1962/07/10", "942357812"),
  (8, "Eva", "San Martin", "1965/08/12", "942241589"),
  (9, "Juan", "Garcia Gomez", "1974/05/15", "942359887")
);